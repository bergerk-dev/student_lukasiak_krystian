package pl.edu.ug.springreactor.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import pl.edu.ug.springreactor.domain.Visit;

@Repository
public interface VisitRepository extends ReactiveMongoRepository<Visit, String> {
}
