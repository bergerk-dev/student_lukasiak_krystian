package pl.edu.ug.servletcalc;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebConfiguration {

    @Bean
    public ServletRegistrationBean<CalcServlet> servletRegistrationCalcBean(){
        return new ServletRegistrationBean<>(new CalcServlet(), "/calc");
    }

}
