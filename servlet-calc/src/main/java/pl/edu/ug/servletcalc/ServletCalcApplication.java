package pl.edu.ug.servletcalc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class ServletCalcApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServletCalcApplication.class, args);
    }

}
