package pl.edu.ug.springmvcwarehouse.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.ug.springmvcwarehouse.domain.Item;
import pl.edu.ug.springmvcwarehouse.service.ItemManager;

import java.util.List;

@RestController
public class ItemController {

    @Autowired
    ItemManager itemManager;

    @GetMapping("/api/item")
    public List<Item> getItems() {
        return itemManager.getAllItems();
    }

    @PostMapping("/api/item")
    Item addItem(@RequestBody Item item) {
        itemManager.addItem(item);
        return item;
    }

    @GetMapping("/api/item/{id}")
    Item getItem(@PathVariable String id) {
        return itemManager.findById(id);
    }

    @DeleteMapping("/api/item/{id}")
    void deleteItem(@PathVariable String id) {
        itemManager.deleteById(id);
    }

    @PutMapping("api/item/{id}")
    Item updateItem(@RequestBody Item item, @PathVariable String id){
       return itemManager.updateItem(id, item);
    }
}
