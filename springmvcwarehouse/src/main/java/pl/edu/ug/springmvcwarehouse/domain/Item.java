package pl.edu.ug.springmvcwarehouse.domain;

import pl.edu.ug.springmvcwarehouse.validation.NoShelfNoPosition;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NoShelfNoPosition
public class Item {
    private String id;

    @NotNull
    @Size(min=2, max=30)
    private String name;

    @NotNull
    private int shelf;

    @NotNull
    private int position;

    @NotNull
    private float value;

    @NotNull
    @Min(1)
    private int weight;

    public Item(String id, String name, int shelf, int position, float value, int weight) {
        this.id = id;
        this.name = name;
        this.shelf = shelf;
        this.position = position;
        this.value = value;
        this.weight = weight;
    }

    public Item() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getShelf() {
        return shelf;
    }

    public void setShelf(int shelf) {
        this.shelf = shelf;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
