package pl.edu.ug.springmvcwarehouse.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pl.edu.ug.springmvcwarehouse.domain.Item;
import pl.edu.ug.springmvcwarehouse.service.ItemManager;

import javax.validation.Valid;

@Controller("itemwebcontroller")
public class ItemController {

    @Autowired
    private ItemManager itemManager;

    @GetMapping("/arrival")
    public String arrivalHome(Model model) {
        model.addAttribute("items", itemManager.getAllItems());
        return "arrival";
    }

    @PostMapping("/item/add")
    public String addItem(@Valid Item item, BindingResult bindingResult, Model model) {

        if(bindingResult.hasErrors()) {
            return "item-add";
        }

        itemManager.addItem(item);
        model.addAttribute("items", itemManager.getAllItems());
        return "arrival";
    }

    @GetMapping("/item/new")
    public String newItem(Model model) {
        model.addAttribute("item", new Item());
        return "item-add";
    }

    @GetMapping("/departure")
    public String departureHome(Model model) {
        model.addAttribute("items", itemManager.getAllItems());
        return "departure";
    }

    @GetMapping("/item/delete/{id}")
    public String deletePerson(@PathVariable("id") String id, Model model) {
        itemManager.deleteById(id);
        model.addAttribute("items", itemManager.getAllItems());
        return "departure";
    }

    @GetMapping("/change")
    public String changeHome(Model model) {
        model.addAttribute("items", itemManager.getAllItems());
        return "change";
    }

    @GetMapping("/item/update/{id}")
    public String itemToChange(@PathVariable("id") String id, Model model) {
        model.addAttribute("item", itemManager.findById(id));
        return "item-update";
    }

    @PostMapping("/item/update")
    public String updateItem(Item item, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()){
            return "item-update";
        }
        itemManager.updateItem(item.getId(), item);
        model.addAttribute("items", itemManager.getAllItems());
        return "change";
    }
}
