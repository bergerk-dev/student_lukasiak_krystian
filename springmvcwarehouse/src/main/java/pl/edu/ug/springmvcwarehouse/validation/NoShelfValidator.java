package pl.edu.ug.springmvcwarehouse.validation;

import pl.edu.ug.springmvcwarehouse.domain.Item;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NoShelfValidator implements ConstraintValidator<NoShelfNoPosition, Item> {
    @Override
    public boolean isValid(Item value, ConstraintValidatorContext context) {
        if(value.getShelf() == 0){
            return value.getPosition() == 0;
        }

        return true;
    }
}
