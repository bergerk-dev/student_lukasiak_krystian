package pl.edu.ug.springmvcjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.ug.springmvcjpa.domain.Visit;

@Repository
public interface VisitRepository extends JpaRepository<Visit, Long> {
}
