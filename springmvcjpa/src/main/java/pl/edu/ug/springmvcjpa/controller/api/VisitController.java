package pl.edu.ug.springmvcjpa.controller.api;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.edu.ug.springmvcjpa.domain.Visit;
import pl.edu.ug.springmvcjpa.service.VisitService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/visit")
public class VisitController {

    private final VisitService visitService;

    @GetMapping
    public List<Visit> getVisits() {
        return visitService.getAll();
    }

    @GetMapping("/{id}")
    public Visit getVisit(@PathVariable Long id) {
        return visitService.getById(id);
    }

    @PostMapping
    public Visit addVisit(@RequestBody Visit visit) {
        return visitService.add(visit);
    }

    @PostMapping("/all")
    public List<Visit> addAllVisits(@RequestBody List<Visit> visits) {
        return visitService.addAll(visits);
    }

    @DeleteMapping("/{id}")
    public void deleteVisit(@PathVariable Long id) {
        visitService.delete(id);
    }

    @PutMapping("/{id}")
    public Visit updateVisit(@PathVariable Long id, @RequestBody Visit visit) {
        return visitService.update(id, visit);
    }
}
