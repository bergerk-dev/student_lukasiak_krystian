package pl.edu.ug.springmvcjpa.controller.web;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.ug.springmvcjpa.domain.Specialization;
import pl.edu.ug.springmvcjpa.service.SpecializationService;

@Controller("specializationweb")
@RequestMapping("/specialization")
@RequiredArgsConstructor
public class SpecializationController {

    private final SpecializationService specializationService;

    @GetMapping
    public String homeSpecialization(Model model) {
        model.addAttribute("specializations", specializationService.getAll());
        return "specialization-home";
    }

    @GetMapping("/form/{id}")
    public String formSpecialization(@PathVariable Long id, Model model) {
        if (id == 0) {
            model.addAttribute("fieldDisabled", "disabled");
        }
        model.addAttribute("specialization", specializationService.getById(id));
        return "specialization-form";
    }

    @PostMapping("/add")
    public String addSpecialization(Specialization specialization, Model model) {
        specializationService.add(specialization);
        model.addAttribute("specializations", specializationService.getAll());
        return "specialization-home";
    }

    @GetMapping("/delete/{id}")
    public String deleteSpecialization(@PathVariable Long id, Model model) {
        specializationService.delete(id);
        model.addAttribute("specializations", specializationService.getAll());
        return "specialization-home";
    }
}
