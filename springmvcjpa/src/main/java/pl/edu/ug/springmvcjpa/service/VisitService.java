package pl.edu.ug.springmvcjpa.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.edu.ug.springmvcjpa.domain.Visit;
import pl.edu.ug.springmvcjpa.repository.VisitRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
public class VisitService {

    private final VisitRepository visitRepository;

    public List<Visit> getAll() {
        return visitRepository.findAll();
    }

    public Visit getById(Long id) {
        return visitRepository.findById(id).orElse(new Visit());
    }

    public Visit add(Visit visit) {
        return visitRepository.save(visit);
    }

    public List<Visit> addAll(List<Visit> visits) {
        return visitRepository.saveAll(visits);
    }

    public Visit update(Long id, Visit visit) {
        visit.setId(id);
        return visitRepository.save(visit);
    }

    public void delete(Long id) {
        if (getById(id) != null) {
            visitRepository.deleteById(id);
        }
    }
}
